//
//  DetailViewController.swift
//  Swift100Project1
//
//  Created by Roman Brazhnikov on 09/03/2019.
//  Copyright © 2019 Roman Brazhnikov. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var Image: UIImageView!
    var selectedImage: String?
    
    var count = 0;
    var currentNumver = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        title = "Picture \(currentNumver + 1) of \(count)"
        
        // share button
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
        
        // Do any additional setup after loading the view.
        if let imageString = selectedImage {
            Image.image = UIImage(named: imageString)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }
    
    @objc func shareTapped(){
        guard let image = Image.image?.jpegData(compressionQuality: 0.8) else {
            print("No image found")
            return
        }
        
        let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
        vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(vc, animated: true)
    }
    

}
